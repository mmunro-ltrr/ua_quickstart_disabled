7.x-1.x-alpha8, 2017-09-14
-----------------------------
- Preparing to tag 7.x-1.0-alpha7 for uaqs_publication.
- UADIGITAL-1062 & UADIGITAL-1063: Adding entity_embed, file entity, file entity inline, and manual_crop.
- Remove the obsolete Bitbucket Piplines configurations from the subtrees.
- UADIGITAL-936 Move publication node settings to default config so site builders can override and still be eligible for updates to this module
- UADIGITAL-972 Due to Backporting, added default pathauto_taxonomy_term_tags_pattern: [term:vocabulary]/[term:name] to feature Unit, Person, and Publication
- UADIGITAL-977 Added dependency to new submodule
- Merged in mmunro-ltrr/uaqs_publication/UADIGITAL-960 (pull request #6)
- Back to dev. [skip ci]

7.x-1.0-alpha7, 2017-08-17
-----------------------------
- UADIGITAL-1062 & UADIGITAL-1063: Adding entity_embed, file entity, file entity inline, and manual_crop.
- Remove the obsolete Bitbucket Piplines configurations from the subtrees.
- UADIGITAL-936 Move publication node settings to default config so site builders can override and still be eligible for updates to this module
- UADIGITAL-972 Due to Backporting, added default pathauto_taxonomy_term_tags_pattern: [term:vocabulary]/[term:name] to feature Unit, Person, and Publication
- UADIGITAL-977 Added dependency to new submodule
- Merged in mmunro-ltrr/uaqs_publication/UADIGITAL-960 (pull request #6)
- Back to dev. [skip ci]

7.x-1.0-alpha6, 2017-01-13
------------------------------
- Merged in trackleft/uaqs_publication/UADIGITAL-884-automate-uaqs-component-re (pull request #5).
- UADIGITAL-884 add bitbucket pipelines config file.
- Merged ua_drupal/uaqs_publication into 7.x-1.x.
- Back to dev.

7.x-1.0-alpha5, 2016-05-02
------------------------------
- Back to dev.

7.x-1.0-alpha4, 2016-04-11
------------------------------
- Merged in trackleft/uaqs_publication/UADIGITAL-580-add-pathauto-module-defaul (pull request #4).
- UADIGITAL-580 add default path auto settings for publication content type.
- Merged ua_drupal/uaqs_publication into 7.x-1.x.
- Merged in trackleft/uaqs_publication/UADIGITAL-535 (pull request #3).
- Adding the s.
- Merged in mmunro-ltrr/uaqs_publication/UADIGITAL-523 (pull request #2).
- Hide the UAQS label in content type names and revise the descriptions.
- Merged in trackleft/uaqs_publication-tmp (pull request #1).
- Prefixing taxonomy.
- Changing research categories to research areas.
- Shorten a field name that was too long, correct a comment.
- Rename UA to UAQS in the file contents.
- Back to dev.

7.x-1.0-alpha3, 2015-10-16
------------------------------
- Back to dev.

7.x-1.0-alpha2, 2015-10-16
------------------------------
- Merged in UADIGITAL-322 (pull request #2).
- UADIGITAL-322: Changes from features module updates.
- Merged in mmunro-ltrr/ua_publication/UADIGITAL-139 (pull request #1).
- Remove explicit version information.
- Add a changelog so there is always something to modify for a release.
- Back to dev.

7.x-1.0-alpha1, 2015-07-10
--------------------------
- Preparing to tag 7.x-1.0-alpha1.
- Renaming README for consistency's sake.
- Changed package name to be consistent with other features.
- Removed project line to prevent complications.
- Keeping w/ conventions: changed version from 7.x-1.0-dev to 7.x-1.x-dev.
- Deleted some core modules I accidentally listed that are not needed.

